import { createI18n } from "vue-i18n";
import locales from "./locale/locales.json";
console.log(123);

/**
 * Load locale messages
 *
 * The loaded `JSON` locale messages is pre-compiled by `@intlify/vue-i18n-loader`, which is integrated into `vue-cli-plugin-i18n`.
 * See: https://github.com/intlify/vue-i18n-loader#rocket-i18n-resource-pre-compilation
 */
// function loadLocaleMessages() {
//   const locales = require.context(
//     "./locale",
//     true,
//     /[A-Za-z0-9-_,\s]+\.json$/i
//   );

//   const messages = {};

//   locales.keys().forEach((key) => {
//     const matched = key.match(/([A-Za-z0-9-_]+)\./i);

//     if (matched && matched.length > 1) {
//       const locale = matched[1];

//       messages[locale] = locales(key);
//     }
//   });
//   return messages;
// }
function loadLocaleMessages() {
  console.log(locales);
  return locales;
}
// loadLocaleMessages();
const i18n = createI18n({
  locale: localStorage.getItem("currentLang")
    ? localStorage.getItem("currentLang")
    : "GE",
  fallbackLocale: "GE",
  messages: locales,
  // messages: {
  //   EN: {
  //     welcomeText: "This is the main page",
  //   },
  //   GE: {
  //     welcomeText: "ეს არის მთავარი გვერდი",
  //   },
  // },
});
export default i18n;
// loadLocaleMessages();
