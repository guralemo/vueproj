import { createStore } from "vuex";

export default createStore({
  state: {
    lang: localStorage.getItem("currentLang")
      ? localStorage.getItem("currentLang")
      : "GE",
  },

  getters: {
    getLang: (state) => {
      return state.lang;
    },
  },
  mutations: {},
  actions: {
    setLang({ state }, lang) {
      state.lang = lang;
    },
  },
  modules: {},
});
